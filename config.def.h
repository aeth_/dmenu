/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */

/* -fn option overrides fonts[0]; default X11 font or font set */
static char tfont[] = "Fira Code:size=11";
static char efont[] = "JoyPixels:pixelsize=10:antialias=true:autohint=true";
static const char *fonts[] = {
	tfont,
	efont,
};

static char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static char normfgcolor[] = "#bbbbbb";
static char normbgcolor[] = "#222222";
static char selfgcolor[]  = "#eeeeee";
static char selbgcolor[]  = "#005577";
static char *colors[SchemeLast][2] = {
	               /*     fg         bg       */
	[SchemeNorm] = { normfgcolor, normbgcolor },
	[SchemeSel]  = { selfgcolor,  selbgcolor  },
	[SchemeOut]  = { "#000000",   "#00ffff" },
	[SchemeHp]   = { "#bbbbbb",   "#333333" }
};
/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines      = 0;

/* -h option; minimum height of a menu line */
static unsigned int lineheight = 0;
static unsigned int min_lineheight = 8;
static unsigned int columns    = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
	{ "dmenu.tfont",       STRING, &tfont },
	{ "dmenu.efont",       STRING, &efont },
	{ "dmenu.normfgcolor", STRING, &normfgcolor },
	{ "dmenu.normbgcolor", STRING, &normbgcolor },
	{ "dmenu.selfgcolor",  STRING, &selfgcolor },
	{ "dmenu.selbgcolor",  STRING, &selbgcolor },
	{ "dmenu.prompt",      STRING, &prompt },
};
